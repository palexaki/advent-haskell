module AoC2021.Day01 (run) where

import Text.Parsec
import Text.Parsec.Char

numberParser :: Parsec String st Int
numberParser = read <$> (many digit)

numbersParser :: Parsec String st [Int]
numbersParser = numberParser `endBy` newline

countIncreases :: [Int] -> Int
countIncreases (x:y:ns)
    | x < y     = 1+(countIncreases (y:ns))
    | otherwise = countIncreases (y:ns)
countIncreases (_:[]) = 0

windows :: [Int] -> [Int]
windows (x:y:z:ns) = x+y+z:windows (y:z:ns)
windows [x, y] = []

run :: String -> IO ()
run contents = do
  let Right numbers = parse numbersParser "" contents
  putStr "Challenge 1: "
  print $ countIncreases numbers
  putStr "Challenge 2: "
  print $ (countIncreases . windows) numbers


