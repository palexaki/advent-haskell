module Main where

import Lib
import AoC2021.Day01
import System.Environment (getArgs)

main :: IO ()
main = do
  args <- getArgs
  [year, day, file] <- if (length args) == 3 then return args else putStrLn "Usage of this program: aocrunner YEAR DAY FILE" *> return []
  fileContents <- readFile file
  case year of
    "2021" -> case day of
      "1" -> AoC2021.Day01.run fileContents
      _ -> putStrLn "This day does not exist"
    _ -> putStrLn "This year does not exist"
